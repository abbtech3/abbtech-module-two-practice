package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    void init(){
        calculator = new Calculator();
    }

    @Test
    void additionTest(){
        int actualResult = calculator.add(10, 5);
        Assertions.assertEquals(actualResult, 15);
    }

    @Test
    void multiplyTest(){
        int actualResult = Calculator.multiple(10, 5);
        Assertions.assertEquals(actualResult, 50);
    }
}
