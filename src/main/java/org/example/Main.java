package org.example;

public class Main {
    public static void main(String[] args) {
        int a = 5;
        int b = 12;
        Calculator calculator = new Calculator();
        System.out.println("Sum of " + a + " and " + b + " is " + calculator.add(a, b));

        System.out.println("Product of " + a + " and " + b + " is " + Calculator.multiple(a, b));
    }
}